package me.hopps.util;

/**
 * 2D Vector class
 * @author Hopps
 *
 */
public class Vector2D {
	
    private double x = 0.0;
    private double y = 0.0;
    
    /**
     * Creates a vector with (0.0|0.0)
     */
    public Vector2D() {
    	
    }
    
    /**
     * Creates a vector with custom coordinates
     * @param x x-coordinate
     * @param y y-coordinate
     */
    public Vector2D( double x, double y ) { 
    	this.x = x; this.y = y; 
    }
    
    /**
     * Creates a vector from another vector
     * @param vec vector which is being copied
     */
    public Vector2D( Vector2D vec ) { 
    	this.x = vec.x; this.y = vec.y; 
    }
    
    
    /**
     * Copies coordinates fomm another vector
     * @param vec vector which is being copied
     */
    public void setTo( Vector2D vec )
    {
        x = vec.x;
        y = vec.y;
    }
    
    /**
     * Changes the coordinates
     * @param x
     * @param y
     */
    public void setTo( double x, double y )
    {
        this.x = x;
        this.y = y;
    }
    
    /**
     * Returns coordniates as a string
     * @return String [x,y]
     */
    public String toString()
    {
        return "[" + (int)x + "," + (int)y + "]";
    }
    
    /**
     * Returns the x-coordinate
     * @return x-coordinate
     */
    public double getX() {
    	return x;
    }
    
    /**
     * Returns the y-coordinate
     * @return y-coordinate
     */
    public double getY() {
    	return y;
    }

    /**
     * Returns the length of the vector
     * @return length
     */
    public double length()
    {
        return Math.sqrt( (x*x) + (y*y) );
    }

    /**
     * Returns a normalized copy of the vector
     * @return Verctor2D
     */
    public Vector2D normalize()
    {
        double len = length();
        double nX, nY;
        nX = x;
        nY = y;

        if( len != 0.0 )
        {
            nX /= len;
            nY /= len;
        }
        else
        {
            nX = 0.0;
            nY = 0.0;
        }

        return new Vector2D(nX, nY);
    }
    
    /**
     * Adds vector to current vector
     * @param vec - added vector
     */
    public void add( Vector2D vec )
    {
        this.x += vec.x;
        this.y += vec.y;
    }
    
    /**
     * Adds custom data to the current vector
     * @param x added to the x-coordinate
     * @param y added to the y-coordinate
     */
    public void add( double x, double y )
    {
        this.x += x;
        this.y += y;
    }

    /**
     * Subs vector from current vector(does not change current vector!)
     * @param vec - subbed vector
     * @return resulting vector
     */
    public Vector2D sub( Vector2D vec )
    {
    	double sX,sY;
    	sX = this.x - vec.x;
    	sY = this.y - vec.y;

        return new Vector2D(sX,sY);
    }
    
    /**
     * Subs custom data to the current vector
     * @param x subbed to the x-coordinate
     * @param y subbed to the y-coordinate
     */
    public void sub( double x, double y )
    {
        this.x -= x;
        this.y -= y;
    }
    
    /**
     * Multiplies vector from current vector(does not change current vector!)
     * @param vec - multiplied vector
     * @return resulting vector
     */
    public Vector2D mul( Vector2D vec )
    {
    	double mX, mY;
    	mX = x;
    	mY = y;
        mX *= vec.x;
        mY *= vec.y;

        return new Vector2D(mX, mY);
    }
    
    /**
     * Multiplies both coordinates with given data(does not change current vector!)
     * @param scalar - multiplier
     */
    public Vector2D mul( double scalar )
    {
    	double mX, mY;
    	mX = x;
    	mY = y;
        mX *= scalar;
        mY *= scalar;
        
        return new Vector2D(mX,mY);
    }
}
