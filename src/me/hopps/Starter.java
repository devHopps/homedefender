package me.hopps;

import me.hopps.homedef.MainGame;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

public class Starter {

	public static void main(String[] args) throws SlickException {
		AppGameContainer app = new AppGameContainer(new MainGame());
	 
	    app.setDisplayMode(800, 600, false);
	    app.start();
	}

}