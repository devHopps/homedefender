package me.hopps.homedef;

import me.hopps.homedef.states.MasterState;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;


public class MainGame extends StateBasedGame{

	public MainGame() {
		super("Homedefender");	
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		this.addState(new MasterState());
		this.enterState(0);
	}
	

}
