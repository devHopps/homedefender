package me.hopps.homedef.states;

import java.util.ArrayList;
import java.util.Random;

import me.hopps.homedef.objects.Bullet;
import me.hopps.homedef.objects.Enemy;
import me.hopps.homedef.objects.Planet;
import me.hopps.homedef.objects.Weapon;

import org.lwjgl.Sys;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;


public class GameState extends BasicGameState {
	
	MasterState mS;
	Planet planet;
	Weapon weapon;
	Enemy testenemy;
	ArrayList<Enemy> enemyList;
	ArrayList<Bullet> BulletList;
	long lastTime, lostTime, lastPeople, time;
	int people;
	double SPEED;
	Sound end, shoot;
	
	public GameState(MasterState lms) {
		mS = lms;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame stg) throws SlickException {
		enemyList = new ArrayList<Enemy>();
		BulletList = new ArrayList<Bullet>();
		planet = new Planet(400,300,100, mS.resource);
		weapon = new Weapon(400,300, mS.resource);
		SPEED = 0.03;
		
		end = mS.resource.getSound("END");
		shoot = mS.resource.getSound("SHOOT");
	}

	@Override
	public void render(GameContainer gc, StateBasedGame stg, Graphics g) throws SlickException {
		g.drawImage(mS.resource.getImage("GAMEBACK"), 0, 0);
		planet.draw(g);
		weapon.draw(gc, g);
		for(int i = 0; i < enemyList.size(); i++) {
			if(!enemyList.get(i).explode)
				enemyList.get(i).draw(g);
		}
		for(int i = 0; i < enemyList.size(); i++) {
			if(enemyList.get(i).explode)
				enemyList.get(i).draw(g);
		}
		for(int i = 0; i < BulletList.size(); i++) {
			BulletList.get(i).draw(g);
		}
		if(planet.HP < 0) {
			g.drawImage(mS.resource.getImage("LOST"), 0, 0);
			if(getTime() - lastTime > 2000) {
				g.drawString("Click to continue",310,350);
				g.drawString("Click to continue",311,350);
			}
		}
		g.drawString("People rescued: " + people,10,575);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame stg, int delta) throws SlickException {
		if(planet.HP > 0) {
			Random rand = new Random();
			weapon.update(gc, delta);
			if(gc.getInput().isMousePressed(0)) {
				shoot.play(1.0f, 0.3f);
				BulletList.add(new Bullet(400 , 300, gc.getInput().getMouseX(), gc.getInput().getMouseY(), 0.12, mS.resource.getAnimation("EXPLO"), mS.resource.getSound("SMALLEX")));
			}
			for(int i = 0; i < enemyList.size(); i++) {
				if(enemyList.get(i).done)
					enemyList.remove(i);
				else
					enemyList.get(i).update(planet, gc, delta);
			}
			for(int i = 0; i < BulletList.size(); i++) {
				if(BulletList.get(i).getX() > 800 || BulletList.get(i).getX() < 0)
					BulletList.remove(i);
				else if(BulletList.get(i).getY() > 600 || BulletList.get(i).getY() < 0)
					BulletList.remove(i);
				else if(BulletList.get(i).used)
					BulletList.remove(i);
				else
					BulletList.get(i).update(enemyList, gc, delta);
			}
			createEnemies();
			
			if(getTime() - lastPeople > 3000+rand.nextInt(5)*1000) {
				lastPeople = getTime();
				people += 10+rand.nextInt(40);
			}
			if(getTime() - time > 7500) {
				time = getTime();
				SPEED += 0.01;
				System.out.println(SPEED);
			}
			
		} else {
			
			if(getTime() - lostTime > 2000) {
				if(gc.getInput().isMousePressed(0)) {
					((LostState) stg.getState(3)).setPeople(people);
					stg.enterState(3);
				}
			}
			
			if(lostTime == 0) {
				lostTime = getTime();
				end.play(1.0f, 0.2f);
			}
			
			for(int i = 0; i < enemyList.size(); i++) {
				if(enemyList.get(i).done)
					enemyList.remove(i);
				else
					enemyList.get(i).update(planet, gc, delta);
			}
			for(int i = 0; i < BulletList.size(); i++) {
				if(BulletList.get(i).getX() > 800 || BulletList.get(i).getX() < 0)
					BulletList.remove(i);
				else if(BulletList.get(i).getY() > 600 || BulletList.get(i).getY() < 0)
					BulletList.remove(i);
				else if(BulletList.get(i).used)
					BulletList.remove(i);
				else
					BulletList.get(i).update(enemyList, gc, delta);
			}
			
		}
	}

	private void createEnemies() {
		Random rand = new Random();
		int s = rand.nextInt(4);
		if(getTime() - lastTime > 500) {
			try {
				if(s == 0) {
					enemyList.add(new Enemy(-50+rand.nextInt(850) , -32, 0.01, SPEED, mS.resource.getImage("ROCKET"), mS.resource.getAnimation("EXPLO"), mS.resource.getSound("BIGEX")));
				}
				if(s == 1) {
					enemyList.add(new Enemy(832 , -50+rand.nextInt(650), 0.01, SPEED, mS.resource.getImage("ROCKET"), mS.resource.getAnimation("EXPLO"), mS.resource.getSound("BIGEX")));
				}
				if(s == 2) {
					enemyList.add(new Enemy(-50+rand.nextInt(850) , 632, 0.01, SPEED, mS.resource.getImage("ROCKET"), mS.resource.getAnimation("EXPLO"), mS.resource.getSound("BIGEX")));
				}
				if(s == 3) {
					enemyList.add(new Enemy(-32 , -50+rand.nextInt(650), 0.01, SPEED, mS.resource.getImage("ROCKET"), mS.resource.getAnimation("EXPLO"), mS.resource.getSound("BIGEX")));
				}
				lastTime = getTime();
			} catch (SlickException e) {
				Log.error("ERROR", e);
			} 
		}
	}

	@Override
	public int getID() {
		return 2;
	}
	
	public long getTime() {
	    return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}

	public void reinit() {
		lastTime = 0; 
		lostTime = 0; 
		lastPeople = 0;
		time = 0;
		people = 0;
		
		enemyList = null;
		BulletList = null;
		planet = null;
		weapon = null;
		
		enemyList = new ArrayList<Enemy>();
		BulletList = new ArrayList<Bullet>();
		planet = new Planet(400,300,100, mS.resource);
		weapon = new Weapon(400,300, mS.resource);
		SPEED = 0.02;
	}

}
