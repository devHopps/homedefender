package me.hopps.homedef.states;

import me.hopps.homedef.menu.Button;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;


public class MenuState extends BasicGameState {
	
	MasterState mS;
	Button start;
	Sound click, starte;
	
	public MenuState(MasterState lms) {
		mS = lms;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame stg) throws SlickException {
		start = new Button(291,135,mS.resource.getImage("START"),mS.resource.getImage("STARTHOVER"));
		click = mS.resource.getSound("CLICK");
		starte = mS.resource.getSound("STARTS");
	}

	@Override
	public void render(GameContainer gc, StateBasedGame stg, Graphics g) throws SlickException {
		g.drawImage(mS.resource.getImage("MENUBACK"), 0, 0);
		g.drawImage(mS.resource.getImage("LOGO"), 100, 0);
		start.draw(gc, g);
		
		g.drawGradientLine(220, 220, Color.white, 220, 460, Color.white);
		g.drawGradientLine(580, 220, Color.white, 580, 460, Color.white);
		g.drawGradientLine(220, 220, Color.white, 580, 220, Color.white);
		g.drawGradientLine(220, 370, Color.white, 580, 370, Color.white);
		g.drawGradientLine(220, 250, Color.white, 580, 250, Color.white);
		g.drawGradientLine(220, 460, Color.white, 580, 460, Color.white);
		g.drawGradientLine(220, 400, Color.white, 580, 400, Color.white);
		g.drawString("About the game", 230, 225);
		g.drawString("Help to defend the planet!", 230, 260);
		g.drawString("You are the only one who is able to", 230, 280);
		g.drawString("controll an ancient weapon.", 230, 300);
		g.drawString("Defend the planet as long as", 230, 320);
		g.drawString("possible, so the people can flee!", 230, 340);
		g.drawString("Controlls", 230, 375);
		g.drawString("Use your mouse to aim,", 230, 410);
		g.drawString("Click to shoot!", 230, 430);
		g.drawString("Made by Benedict Balzer, April 2012", 230, 470);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame stg, int delta) throws SlickException {
		start.checkHovered(gc.getInput().getMouseX(), gc.getInput().getMouseY());
		if(gc.getInput().isMouseButtonDown(0)) {
			if(start.clicked())
				click.play(1.0f, 0.2f);
				starte.play(1.0f, 0.3f);
				stg.enterState(2);
		}
	}

	@Override
	public int getID() {
		return 1;
	}

}
