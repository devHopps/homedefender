package me.hopps.homedef.states;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import me.hopps.util.ResourceManager;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.loading.DeferredResource;
import org.newdawn.slick.loading.LoadingList;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;


public class MasterState extends BasicGameState {
	
	ResourceManager resource;
	private DeferredResource nextResource;

	@Override
	public void init(GameContainer gc, StateBasedGame stg) throws SlickException {
		try {
			InputStream input = new FileInputStream("res/res.xml");
			resource = new ResourceManager();
			resource.loadResources(input, true);
		} catch (FileNotFoundException e) {
			Log.error("Couldn't load resources", e);
		}
	}

	@Override
	public void render(GameContainer gc, StateBasedGame stg, Graphics g) throws SlickException {
		int total = LoadingList.get().getTotalResources();
		int loaded = LoadingList.get().getTotalResources() - LoadingList.get().getRemainingResources();
		
		g.fillRect(220,330,loaded*387/LoadingList.get().getTotalResources(),40);
		g.setColor(Color.red);
		g.drawRect(220,330,total*387/LoadingList.get().getTotalResources(),40);
		g.setColor(Color.red);
		float baru = loaded / (float) total * 100;
		int bar = Math.round(baru-1);
		g.drawString( bar + "%", 395, 344);
		g.setColor(Color.white);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame stg, int delta) throws SlickException {
		if (nextResource != null) {
			try {
				nextResource.load();
			} catch (IOException e) {
				Log.error("Couldn't load resource", e);
			}
			
			nextResource = null;
		}
		
		if (LoadingList.get().getRemainingResources() > 0) {
			nextResource = LoadingList.get().getNext();
		} else {
			MenuState menu = new MenuState(this);
			menu.init(gc, stg);
			
			GameState game = new GameState(this);
			game.init(gc, stg);
			
			LostState lost = new LostState(this);
			lost.init(gc, stg);
			
			stg.addState(menu);
			stg.addState(game);
			stg.addState(lost);
			
			stg.enterState(1);
		}
	}

	@Override
	public int getID() {
		return 0;
	}

}
