package me.hopps.homedef.states;

import me.hopps.homedef.menu.Button;

import org.lwjgl.Sys;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;


public class LostState extends BasicGameState {
	
	MasterState mS;
	Button start;
	int people;
	boolean first;
	float time;
	Sound click;
	
	public LostState(MasterState lms) {
		mS = lms;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame stg) throws SlickException {
		start = new Button(291,135,mS.resource.getImage("START"),mS.resource.getImage("STARTHOVER"));
		click = mS.resource.getSound("CLICK");
	}

	@Override
	public void render(GameContainer gc, StateBasedGame stg, Graphics g) throws SlickException {
		g.drawImage(mS.resource.getImage("MENUBACK"), 0, 0);
		g.drawImage(mS.resource.getImage("LOGO"), 100, 0);
		start.draw(gc, g);
		g.setColor(Color.red);
		g.drawString("You successfully rescued " + people + " people", 230, 225);
		g.setColor(Color.white);
		g.drawString("Click the button top play again!", 230, 260);
		g.drawString("Thank you for playing HomeDefender", 230, 300);
		g.drawString("Made by Benedict Balzer, April 2012", 230, 460);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame stg, int delta) throws SlickException {
		if(!first) {
			time = getTime();
			((GameState) stg.getState(2)).reinit();
			first = true;
			System.out.println("REINIT");
		}
		start.checkHovered(gc.getInput().getMouseX(), gc.getInput().getMouseY());
		
		if(getTime() - time > 500) {
			if(gc.getInput().isMouseButtonDown(0)) {
				if(start.clicked())
					click.play(1.0f, 0.2f);
					stg.enterState(2);
				first = false;
			}
		}
		
	}
	
	public long getTime() {
	    return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}

	@Override
	public int getID() {
		return 3;
	}
	
	public void setPeople(int peop) {
		people = peop;
	}

}
