package me.hopps.homedef.objects;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;

import me.hopps.util.ResourceManager;

public class Planet {

	private ResourceManager res;
	Circle body;
	public int HP = 10;

	public Planet(int x, int y, int r, ResourceManager resource) {
		body = new Circle(x,y,r);
		res = resource;
	}
	
	public void update(GameContainer gc, int delta) {
		
	}
	
	public void draw(Graphics g) {
		g.texture(body, res.getImage("PLANET"), true);
	}

}
