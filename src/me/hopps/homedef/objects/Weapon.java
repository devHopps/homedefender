package me.hopps.homedef.objects;

import me.hopps.util.ResourceManager;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;


public class Weapon {
	
	private Image weaponImage;
	private double targetAng;
	
	
	public Weapon(int x, int y, ResourceManager resource) {
		weaponImage = resource.getImage("WEAPON");
	}

	public void update(GameContainer gc, int delta) {
		this.setTarget(gc.getInput().getMouseX(), gc.getInput().getMouseY());
	}

	public void draw(GameContainer gc, Graphics g) throws SlickException {
		weaponImage.rotate((float) targetAng);
		weaponImage.draw(400-weaponImage.getHeight()/2, 300-weaponImage.getWidth()/2);
		weaponImage.rotate((float) -targetAng);

	}
	
	public void setTarget(float pX, float pY) {
		targetAng = (float) getTargetAngle(400, 300, pX, pY);
	}
	
	public float getTargetAngle(float startX, float startY, float targetX, float targetY) {
		float dx = targetX - startX;
		float dy = targetY - startY;
		return (float)Math.toDegrees(Math.atan2(dy, dx));
	}

}
