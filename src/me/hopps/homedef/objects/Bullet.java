package me.hopps.homedef.objects;

import java.util.ArrayList;

import me.hopps.util.Vector2D;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.geom.Circle;


public class Bullet {
	
	private Vector2D targetPosition, MovementVector, MovementVectorNorm, pos;
	private double speed;
	private Circle circle;
	public boolean used;
	Sound explo;

	public Bullet(int pX, int pY,int zX, int zY, double pMaxSpeed, Animation ex, Sound explos) throws SlickException {
	    targetPosition = new Vector2D(pX , pY);
	    pos = new Vector2D(400,300);
	    this.setTarget(zX,zY);
	    speed = pMaxSpeed;
	    circle = new Circle(399,299,2);
	    explo = explos;
	    
	    Vector2D centerPos = new Vector2D(circle.getCenterX(), circle.getCenterY());
	    MovementVector = targetPosition.sub(centerPos);
	    MovementVectorNorm = MovementVector.normalize();
	}
	
	public void update(ArrayList<Enemy> enemyList, GameContainer gc, int delta) {
		for(int i = 0; i < enemyList.size(); i++) {
			if(enemyList.get(i).getShape().intersects(circle) && !enemyList.get(i).miniex) {
				explo.play(1.0f, 0.3f);
				used = true;
				enemyList.get(i).miniex = true;
				enemyList.get(i).explode = true;
			}
		}
		pos.add(MovementVectorNorm.mul(speed*delta));
		circle.setX((float) pos.getX());
		circle.setY((float) pos.getY());
	}

	public void draw(Graphics g) throws SlickException {
		g.fill(circle);
	}
	
	private void setTarget(double pX, double pY) {
		targetPosition.setTo(pX, pY);
	}

	public double getX() {
		return pos.getX();
	}
	
	public double getY() {
		return pos.getY();
	}
}
