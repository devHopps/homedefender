package me.hopps.homedef.objects;

import me.hopps.util.Vector2D;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.geom.Rectangle;

public class Enemy {
	
	private Image shipImage;
	private Vector2D shipPosition, targetPosition, CenterShip;
	private double targetAng, acceleration, maxSpeed, speed;
	public boolean reached, explode;
	private Rectangle rect;
	private Animation explo;
	public boolean miniex, done;
	Sound explosion;
	

	public Enemy(int pX, int pY, double pAcceleration, double pMaxSpeed, Image img, Animation ex, Sound explos) throws SlickException {
		acceleration = pAcceleration;
		maxSpeed = pMaxSpeed;
	    shipPosition = new Vector2D(pX , pY);
	    targetPosition = new Vector2D(pX , pY);
	    this.setTarget(400,300);
	    CenterShip = new Vector2D();
	    shipImage = img;
	    rect = new Rectangle(0, 0, 32, 32);
	    explo = ex;
	    explo.setLooping(false);
	    explosion = explos;
	}
	
	public void update(Planet planet, GameContainer gc, int delta) {
		if(!explode) {
			if(!reached && (targetPosition.getX() != shipPosition.getX()+(double)shipImage.getWidth()/2 || targetPosition.getY()+(double)shipImage.getHeight()/2 != shipPosition.getY()) ) {
				CenterShip.setTo(shipPosition.getX()+(double)shipImage.getWidth()/2, shipPosition.getY()+(double)shipImage.getHeight()/2);
				Vector2D MovementVector = targetPosition.sub(CenterShip);
				Vector2D MovementVectorNorm = MovementVector.normalize();
				if(speed < maxSpeed && MovementVector.length() > speed/acceleration*1.1){
					speed += acceleration;
				} else {
					speed -= acceleration;
				}
				if(MovementVector.length() < 10) {
					   reached = true;
					   speed = 0;
				}
				shipPosition.add(MovementVectorNorm.mul(speed*delta));
			}
			rect.setLocation((float)shipPosition.getX(), (float)shipPosition.getY());
			if(planet.body.intersects(rect)) {
				explode = true;
				planet.HP--;
				explosion.play(1.0f, 0.3f);
			}
		} else {
			explo.update(delta);
		}
		if(explo.isStopped() && explode) {
			done = true;
		}
	}

	public void draw(Graphics g) throws SlickException {
		if(!explode) {
			shipImage.rotate((float) targetAng);
			shipImage.draw((int) shipPosition.getX(), (int) shipPosition.getY());
			shipImage.rotate((float) -targetAng);
		} else {
			if(this.miniex) {
				float x = (float) shipPosition.getX();
				float y = (float) shipPosition.getY();
				explo.draw(x-16, y-16, 64, 64);
			} else {
				float x = (float) shipPosition.getX();
				float y = (float) shipPosition.getY();
				float h = 64;
				if(x-h/2 < 400 && y-h/2 < 300)
					explo.draw(x,y);
				else
					explo.draw(x-h,y-h);
			}
		}
		
	}
	
	private void setTarget(double pX, double pY) {
		targetPosition.setTo(pX, pY);
		targetAng = (float) getTargetAngle((float)shipPosition.getX(), (float)shipPosition.getY(), (float)targetPosition.getX(), (float)targetPosition.getY());
		reached = false;
	}
	
	private float getTargetAngle(float startX, float startY, float targetX, float targetY) {
		float dx = targetX - startX;
		float dy = targetY - startY;
		return (float)Math.toDegrees(Math.atan2(dy, dx));
	}
	
	public Rectangle getShape() {
		return rect;
	}


}
