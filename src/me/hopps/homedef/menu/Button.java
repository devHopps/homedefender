package me.hopps.homedef.menu;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class Button {
	
	public int x,y;
	Image normal, hover;
	boolean hovered;
	
	public Button(int x, int y, Image normal, Image hover) {
		this(x,y,normal);
		this.hover = hover;
	}
	
	public Button(int x, int y, Image normal) {
		this.x = x;
		this.y = y;
		this.normal = normal;
	}
	
	public void draw(GameContainer gc, Graphics g) {
		if(hovered && hover != null)
			hover.draw(x, y);
		else
			normal.draw(x, y);
	}

	public void checkHovered(int mouseX, int mouseY) {
		if(x < mouseX && y < mouseY && x+218 > mouseX && y+60 > mouseY)
			hovered = true;
		else
			hovered = false;
	}

	public boolean clicked() {
		if(hovered)
			return true;
		else
			return false;
	}

}
