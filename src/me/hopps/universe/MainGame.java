package me.hopps.universe;

import me.hopps.universe.states.MasterState;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class MainGame extends StateBasedGame {
    
    MasterState mS;
    
    public MainGame() {
        super("Homedefender");
    }

    @Override
    public void initStatesList(GameContainer gc) throws SlickException {
        mS = new MasterState();
        //mS.init(gc, this);
        
        this.addState(mS);
    }
    
}
